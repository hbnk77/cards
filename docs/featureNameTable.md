|english|german| .cards 3.0 (german) | code | database|
|-------|------|---------------------|------|---------|
| card |Karte |Karte |card | cards |
| cardset | Kartei | Kartensatz | cardset | cardsets |
| repetitorium | Repetitorium | Gemischter Kartensatz | repetitorium / cardset.shuffled | cardsets |
| transcript | lose Mitschrift | -  | transcritp | cards |
| transcript bonus | gekoppelte Mitschrift | - | transcript | transcriptBonus |
| leitner | Leitner | Lernkartei | leitner / box | leitner |
| wozniak | Wozniak |  Memo | wozniak / memo | wozniak |
| learning log | Lernverlauf | - | learningLog | leitnerHistory|
| learning history | Lernprotokoll | - | learningHistory | leitnerHistory |
| task | Freischaltungstag | Termin / Aufgabe | task | leitnerTasks |
| workload | Lernpensum | workload | workload | workload |
| card type | Karteityp | Kartentyp | cardType | - |
| user | Account | Benutzer | user | users |
| cardset index | Kartenverzeichnis | - |cardsetIndex | - |
| wordcloud | Wortkwolke | - | wordcloud | wordcloud |
| ratings | Bewertung | Bewertung | rating | ratings |
| presentation | Kartenschau | - | presentation | - |
| card index | Kartenverzeichnis | Liste | cardIndex | Cards |
| pool | Themenpool | Pool | filter (Includes pool, my cardsets, workload, transcripts, all cardsets) | cards, cardsets, workload |
| notifications | Benachrichtigungen | Benachrichtigungen | notifications | notifications|
| learning status | Lernstand |  - | graph / learningStatus | - |
| pomodoro | Pomodoro | - | pomodoro | - |
| use cases | Hauptziele | - | useCases | - |
